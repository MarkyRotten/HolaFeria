const { app, BrowserWindow } = require('electron')

let windowInstance;
let createdWindow = null;
const gotTheLock = app.requestSingleInstanceLock();

createWindow = () => {
  // Create the browser window.
  windowInstance = new BrowserWindow({
    width: 1024,
    height: 800,
    backgroundColor: '#ffffff',
    icon: `file://${__dirname}/dist/assets/logo.png`
  });

  windowInstance.loadURL(`file://${__dirname}/dist/index.html`);

  // Event when the window is closed.
  windowInstance.on('closed', function () {
    windowInstance = null
  })
}

if (!gotTheLock) {
  app.quit()
} else {
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (createdWindow) {
      if (createdWindow.isMinimized()) createdWindow.restore()
      createdWindow.focus()
    }
  })

  // Create createdWindow, load the rest of the app, etc...
  app.on('ready', createWindow)

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {

    // On macOS specific close process
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })

  app.on('activate', () => {
    // macOS specific close process
    if (windowInstance === null) {
      createWindow()
    }
  })
}
