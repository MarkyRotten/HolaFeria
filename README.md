# HolaFeria
Sistema web/escritorio para el manejo de stock, ventas y estadísticas de una feria americana.

## Desarrolladores
- [Marcos José Cosio Collias](https://gitlab.com/MarkyRotten) (Desarrollador BE)
- [Nicolás Omar González Passerino](https://gitlab.com/NicolasOmar) (Desarrollador UI)

## Lugar de pruebas
Para hacer una prueba del sistema, ingrese al [sitio de pruebas](https://hola-feria.herokuapp.com) que tiene la ultima version deployada.
 - La contraseña para ingresar como Administrador es 'test'.
 - La contraseña para ingresar como Vendedor es 'idea'.

## Ramas
 - master: Usado para implementar mejoras varias partiendo del primer release (la version final para el cliente).
 - release-1: Usada como rama que guardará la versión entregada al cliente.

## Tecnologías usadas
 - [Angular](https://cli.angular.io/) v7.0.5
 - [Bootstrap](https://getbootstrap.com/) v4.1.3
 - [Jquery](https://jquery.com/) v3.1.1
 - [Moment](https://momentjs.com/) v2.24
 - [Firebase](https://firebase.google.com) v5.5.8
 - [Electron](https://electronjs.org/) v6.0.3

## Instalación
 - Primero instale los paquetes de node (tanto los globales y como los propios del proyecto)
```
npm run setup
```
 - Para iniciar en modo angular (abriendo una página automaticamente):
```
npm run web
```
 - Para inicar en modo electron (iniciandola en modo escritorio):
```
npm run electron
```
 - Para crear una build del sistema e iniciarlo en modo escritorio (en este ejemplo, para Windows):
```
npm run build-windows
```

## Posibles bloqueos
 - Problema: Me sale un error relacionado al node-sass
 - Solución: Ejecutá el siguiente comando para instalar node-sass (incluir 'sudo' al principio en caso de trabajar en linux)
 ```
 npm install node-sass
 ```
 - Problema: No puedo iniciar la aplicación en Linux
 - Solución: Ejecute el siguiente comando antes de iniciar en modo electron (incluir 'sudo' al principio en caso de trabajar en linux)
 ```
 xhost +si:localuser:root
 ``` 
 - Problema: No puedo instalar modulos en Linux aun ejecutando con 'sudo'
 - Solución: Ejecute el siguiente comando cambiando 'MODULE_NAME' por el nombre del paquete que quiera instalar
 ```
 sudo npm install MODULE_NAME --save-dev --unsafe-perm=true --allow-root
 ```
