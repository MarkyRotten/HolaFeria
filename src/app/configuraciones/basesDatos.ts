export default {
  ARTICULOS: 'articulos',
  VENTAS: 'ventas',
  ARTICULOS_ELIM: 'articulos_eliminados',
  ESTADISTICAS: 'estadisticas',
  JORNADAS: 'jornadas',
  MEDIOS_PAGO: 'medios_pago',
  PRECIOS_MOD: 'precios_modificados',
  TIPOS_ARTICULOS: 'tipo_articulos',
  USUARIOS: 'usuarios'
}
