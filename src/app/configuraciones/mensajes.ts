const ingresePass = `, ingrese su contraseña de usuario.`,
      holaFeria = `¡HolaFeria!`;

export default {
  darAcceso: `Para acceder a esta sección${ingresePass}`,
  realizarAccion: `Para realizar esta acción${ingresePass}`,
  iniciarJornada: `Para iniciar una nueva jornada${ingresePass}`,
  terminarJornada: `Para terminar esta jornada${ingresePass}`,
  terminarVenta: `Para finalizar esta venta${ingresePass}`,
  registrarStock: `Para confirmar el ingreso de nuevos artículos al stock${ingresePass}`,
  cambiarPrecioArticulo: `Para cambiar el precio de este artículo${ingresePass}`,
  eliminarArticulo: `Para eliminar este artículo del stock${ingresePass}`,
  eliminarVenta: `Para eliminar esta venta de la jornada${ingresePass}`,
  registrarUsuario: `Para confirmar el registro${ingresePass}`,
  guardarUsuario: `Para confirmar los cambios de este usuario${ingresePass}`,
  faltanCampos: `¡Ups! ¿Revisaste los campos en rojo?`,
  errorUsuario: `¡Ups! ¿Revisaste los campos en rojo? ¿Y si pensás en otra contraseña?`,
  errorStock: `¡Ups! ¿Revisaste los campos en rojo? ¿Y si los códigos están mal escritos?`,
  errorJornada: `¡Ups! Al parecer no hay una jornada iniciada.`,
  accesoDenegado: `¡Ups! Su usuario no está registrado o se encuentra bloqueado. Para más información, comuníquese con el administrador.`,
  tituloNecesitaPass: `${holaFeria} te pide contraseña`,
  tituloErrorAcceso: `${holaFeria} no puede continuar`
};
