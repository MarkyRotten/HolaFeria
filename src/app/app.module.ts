import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
// ANGULAR => CONEXION CON FIREBASE
import { environment } from '../environments/environment.prod';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
// MODULO => MODAL DE BOOTSTRAP
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// MODULO => RUTEO DE ANGULAR
import { RoutingModule } from './app-routing.module';
// MODULOS => SECCIONES DEL SISTEMA
import { EncabezadoModule } from './modulos/encabezado.module';
import { EstadisticasModule } from './modulos/estadisticas.module';
import { StockModule } from './modulos/stock.module';
import { UsuariosModule } from './modulos/usuarios.module';
import { VentasModule } from './modulos/ventas.module';
// MODULO => GUARDAS
import { GuardasModule } from './guardas/guardas.module';
// MODULO => SERVICIOS DE BACK END
import { FirebaseModule } from './firebase/firebase.module';
// MODULO => SERVICIOS DE FRONT END
import { ServiciosModule } from './servicios/servicios.module';
// COMPONENTES => APP COMPONENT
import { AppComponent } from './app.component';
// COMPONENTES => MODAL
import { ModalComponent } from './componentes/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    RoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    FirebaseModule,
    GuardasModule,
    ServiciosModule,
    EncabezadoModule,
    EstadisticasModule,
    StockModule,
    UsuariosModule,
    VentasModule
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent]
})
export class AppModule { }
