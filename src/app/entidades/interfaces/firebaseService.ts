import { AngularFireList, AngularFireDatabase } from "angularfire2/database";

export class FirebaseService {
  protected listado: AngularFireList<any>;

  constructor(
    protected nombreBd: string,
    public firebase: AngularFireDatabase,
  ) {
    this.listado = this.firebase.list(this.nombreBd)
  }

  buscar_bd() {
    return this.listado.snapshotChanges();
  }

  insertar_item_bd(item: any, esMock: boolean = false) {
    this.listado.push(
      esMock ? item : item.toJS()
    );
  }

  modificar_item_bd(item: any) {
    this.listado.update(
      item.$key, item.toJS()
    )
  }

  crear_bd(datosPorCargar: Array<any>) {
    datosPorCargar.forEach(
      _item => {
        this.insertar_item_bd(_item, true);
      }
    )
  }

  eliminar_bd() {
    this.listado.remove()
  }
}
