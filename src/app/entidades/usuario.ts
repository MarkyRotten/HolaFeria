// IMPORTAR TIPOS
import { Texto } from './tipos/texto';
import { Numero } from './tipos/numero';
import { Fecha } from './tipos/fecha';
import { Booleano } from './tipos/booleano';

const usuarioDummy = {
  estado: true
};

export class Usuario {
  nombre: Texto;
  pass: Texto;
  tipoUsuario: Texto;
  sueldo: Numero;
  estado: Booleano;
  // FECHAS
  creadoEl: Fecha;
  bloqueadoEl: Fecha;
  ultimoAcceso: Fecha;
  // VARIABLES NO ENVIADAS A BE
  $key: string;
  id: number;
  passNueva: Texto;
  passComparar: Texto;
  editando: boolean;

  constructor(_obj: any = usuarioDummy) {
    const {
      nombre,
      pass,
      tipoUsuario,
      sueldo,
      estado,
      creadoEl,
      bloqueadoEl,
      ultimoAcceso,
      $key,
      id
    } = _obj;

    this.nombre = new Texto(nombre);
    this.pass = new Texto(pass);
    this.tipoUsuario = new Texto(tipoUsuario);
    this.sueldo = new Numero(sueldo);
    this.estado = new Booleano(estado);
    // FECHAS
    this.creadoEl = new Fecha(creadoEl);
    this.bloqueadoEl = new Fecha(bloqueadoEl);
    this.ultimoAcceso = new Fecha(ultimoAcceso);
    // VARIABLES NO ENVIADAS A BE
    this.$key = $key || null;
    this.id = id || 0;
    this.passComparar = new Texto(null);
    this.passNueva = new Texto(null);
    this.editando = false;
  }

  dameEstado(): string {
    return this.estado.valor ? 'Habilitado' : 'Bloqueado';
  }

  esVendedor(): boolean {
    return this.tipoUsuario.valor === 'Vendedor';
  }

  cambiarEdicion(): void {
    this.editando = !this.editando;
  }

  get esValido(): boolean {
    return this.nombre.esValido &&
    this.tipoUsuario.esValido &&
    this.sueldo.esValido &&
    this.passOk;
  }

  get esValidoMod(): boolean {
    return this.nombre.esValido &&
      this.tipoUsuario.esValido &&
      this.sueldo.esValido &&
      this.modPassOk;
  }

  get passOk(): boolean {
    return this.pass.esValido &&
      this.passComparar.esValido &&
      this.pass.valor === this.passComparar.valor;
  }

  get modPassOk(): boolean {
    return !this.pass.esValido ||
      (
        this.pass.valor === this.pass.valorPrevio &&
        this.passNueva.esValido &&
        this.pass.valor !== this.passNueva.valor &&
        this.passNueva.valor === this.passComparar.valor
      );
  }

  toJS(): object {
    return {
      'nombre': this.nombre.valor,
      'pass': this.pass.valor,
      'tipoUsuario': this.tipoUsuario.valor,
      'sueldo': this.sueldo.valor,
      'estado': this.estado.valor,
      'creadoEl': this.creadoEl.valorPrevio,
      'bloqueadoEl': this.bloqueadoEl.valorPrevio,
      'ultimoAcceso': this.ultimoAcceso.valorPrevio
    };
  }
}
