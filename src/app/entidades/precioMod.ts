import { Fecha } from "./tipos/fecha";
import { Texto } from "./tipos/texto";

// IMPORTAR ENTIDADES

const precioModDummy = {
  fecha: null,
  articulo: null,
  precioViejo: null,
  precioNuevo: null,
  autorizacion: null,
  descripcion: null,
}

export class PrecioMod {
  fecha: Fecha;
  articulo: string;
  precioViejo: number;
  precioNuevo: number;
  autorizacion: string;
  descripcion: Texto;

  constructor(_obj: any = precioModDummy) {
    this.fecha = new Fecha(_obj.fecha);
    this.articulo = _obj.articulo;
    this.precioViejo = _obj.precioViejo;
    this.precioNuevo = _obj.precioNuevo;
    this.autorizacion = _obj.autorizacion;
    this.descripcion = new Texto(_obj.descripcion);
  }

  toJS() {
    return {
      'fecha': this.fecha.valor,
      'articulo': this.articulo,
      'precioViejo': this.precioViejo,
      'precioNuevo': this.precioNuevo,
      'autorizacion': this.autorizacion,
      'descripcion': this.descripcion.valor
    }
  }
}
