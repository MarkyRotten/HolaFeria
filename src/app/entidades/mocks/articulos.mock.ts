export default [
  {
    'codigo': 'SW1',
    'descripcion': 'reprehenderit nisi',
    'precioStock': '266.89',
    'precioVenta': '266.89',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW2',
    'descripcion': 'elit qui',
    'precioStock': '324.58',
    'precioVenta': '324.58',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW3',
    'descripcion': 'sit nisi',
    'precioStock': '311.58',
    'precioVenta': '311.58',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW4',
    'descripcion': 'velit qui',
    'precioStock': '66.89',
    'precioVenta': '66.89',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW5',
    'descripcion': 'ullamco ex',
    'precioStock': '52.49',
    'precioVenta': '52.49',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW6',
    'descripcion': 'duis proident',
    'precioStock': '451.33',
    'precioVenta': '451.33',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW7',
    'descripcion': 'proident mollit',
    'precioStock': '81.74',
    'precioVenta': '81.74',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW8',
    'descripcion': 'dolor ea',
    'precioStock': '448.62',
    'precioVenta': '448.62',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW9',
    'descripcion': 'ea commodo',
    'precioStock': '422.46',
    'precioVenta': '422.46',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW10',
    'descripcion': 'dolore cupidatat',
    'precioStock': '109.8',
    'precioVenta': '109.8',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW11',
    'descripcion': 'exercitation aliqua',
    'precioStock': '491.45',
    'precioVenta': '491.45',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW12',
    'descripcion': 'exercitation et',
    'precioStock': '58.72',
    'precioVenta': '58.72',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW13',
    'descripcion': 'nisi sit',
    'precioStock': '66.13',
    'precioVenta': '66.13',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW14',
    'descripcion': 'tempor ullamco',
    'precioStock': '79.21',
    'precioVenta': '79.21',
    'descuento': '0',
    'mostrar': true
  },
  {
    'codigo': 'SW15',
    'descripcion': 'tempor ullamco',
    'precioStock': '85.21',
    'precioVenta': '85.21',
    'descuento': '0',
    'mostrar': true
  }
];
