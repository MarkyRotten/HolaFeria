// IMPORTAR TIPOS
import { Codigo } from './tipos/codigo';
import { Texto } from './tipos/texto';
import { Numero } from './tipos/numero';
import { Decimal } from './tipos/decimal';

export class Articulo {
  codigo: Codigo;
  descripcion: Texto;
  precioStock: Numero;
  precioVenta: Numero;
  descuento: Decimal;
  // VARIABLES NO ENVIADAS A BE
  $key: string;
  mostrar: boolean;
  cambiarPrecio: boolean;
  id: number;
  fechaVenta: string;
  vendedor: string;

  constructor(_obj: any) {
    if (!_obj) {
      _obj = {
        descuento: '0',
        mostrar: true
      };
    }

    this.codigo = new Codigo(_obj.codigo);
    this.descripcion = new Texto(_obj.descripcion);
    this.precioStock = new Numero(_obj.precioStock);
    this.precioVenta = new Numero(_obj.precioVenta);
    this.descuento = new Decimal(_obj.descuento);
    // VARIABLES NO ENVIADAS A BE
    this.$key = _obj ? _obj.$key : null;
    this.mostrar = true;
    this.cambiarPrecio = false;
    this.id = _obj.id;
    this.fechaVenta = _obj.fechaVenta;
    this.vendedor = _obj.vendedor;
  }

  get esValido(): boolean {
    return Boolean(this.codigo.esValido &&
      this.descripcion.esValido &&
      this.precioStock.esValido &&
      this.descuento.esValido);
  }

  get calcularPrecioVenta() {
    const _descuento = +this.descuento.valor > 0 ?
      (100 - this.descuento.valor) / 100 : 1;

    return Number(+this.precioStock.valor * _descuento).toFixed(1);
  }

  get truncarCodigo(): boolean {
    return this.codigo.esValido && this.descripcion.valor;
  }

  get darCodigoNombre(): string {
    return `${this.codigo.valor} - ${this.descripcion.valor}`;
  }

  restaurarPrecio() {
    this.precioStock.valor = this.precioStock.valorPrevio;
  }

  actualizarPrecio() {
    this.precioStock.valorPrevio = this.precioStock.valor
  }

  toJS() {
    return {
      'codigo': this.codigo.valor,
      'descripcion': this.descripcion.valor,
      'precioStock': this.precioStock.valor,
      'precioVenta': this.calcularPrecioVenta,
      'descuento': this.descuento.valor
    };
  }
}
