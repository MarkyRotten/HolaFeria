// ESLLAMADAVALIDA: RECIBO UN BOOLEANO QUE CORRESPONDE A UNA VALIDACION DEL OBJETO QUE ESTOY POR PASAR. SI DEVUELVE FALSO, DEBERIA APARECERTE UN MSJ DE ERROR
// NECESITAACCESO: SI PASA LA VALDACION DE 'ESVALIDO', PREGUNTO SI NECESITA UNA PASS PARA CONTINUAR
// SOLOADMIN: EL USUARIO DEBE TENER UN ROL ADMINISTRADOR
// MSJMODAL: QUE MENSAJE APARECERA EN EL MODAL CUANDO PIDAS LA CONTRASEÑA
// SERVICIO: CUANDO TERMINES LAS VALDACIONES ANTERIORES, A QUE SERVICIO Y METODO DE UI VAS A LLAMAR
// RUTAFINAL: UNA VEZ HECHO TODOS LOS PASOS ANTERIORES, A QUE RUTA REDIGIDIRAS AL USUARIO

export class ModalConfig {
  esLlamadaValida: boolean;
  msjError: any;
  necesitaAcceso: boolean;
  soloAdmin: boolean;
  msjModal: any;
  servicio: any;
  rutaFinal: any;

  constructor(_obj: any = {}) {
    this.esLlamadaValida = _obj.esLlamadaValida !== undefined ? _obj.esLlamadaValida : true;
    this.msjError = _obj.msjError || null;
    this.necesitaAcceso = _obj.necesitaAcceso || false;
    this.soloAdmin = _obj.soloAdmin || false;
    this.msjModal = _obj.msjModal || null;
    this.servicio = _obj.servicio || null;
    this.rutaFinal = _obj.rutaFinal || null;
  }
}
