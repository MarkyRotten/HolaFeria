// IMPORTAR TIPOS
import { Decimal } from './tipos/decimal';
import { Codigo } from './tipos/codigo';
import { Texto } from './tipos/texto';

export class Descuento {
  codigo: Codigo;
  descripcion: Texto;
  descuento: Decimal;

  constructor(_obj: any) {
    if (!_obj) {
      _obj = {};
    }

    this.codigo = new Codigo(_obj.codigo);
    this.descripcion = new Texto(_obj.descripcion)
    this.descuento = new Decimal(_obj.descuento);
  }

  get esValido(): boolean {
    return Boolean(this.codigo.esValido && this.descuento.esValido);
  }

  toJS() {
    return {
      'codigo': this.codigo.valor,
      'descripcion': this.descripcion.valor,
      'descuento': this.descuento.valor
    };
  }
}
