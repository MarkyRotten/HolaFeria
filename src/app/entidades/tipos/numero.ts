import { Entidad } from './entidad';

export class Numero extends Entidad {
  constructor(_valor: any) {
    super(_valor);
  }

  get esValido(): Boolean {
    const _valor = +this.valor;

    return !isNaN(_valor) &&
        this.valor !== '' &&
        this.valor !== null &&
        this.valor !== undefined &&
        _valor >= 0;
  }
}
