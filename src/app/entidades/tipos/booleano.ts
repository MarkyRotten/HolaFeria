import { Entidad } from './entidad';

export class Booleano extends Entidad {
  constructor(_valor: any) {
    super(_valor);
  }

  get esValido(): Boolean {
    return this.valor;
  }
}
