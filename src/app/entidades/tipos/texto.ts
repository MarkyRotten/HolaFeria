import { Entidad } from './entidad';

export class Texto extends Entidad {
  constructor(_valor: any) {
    super(_valor);
  }

  get esValido(): Boolean {
    return Boolean(this.valor) && this.valor.length > 0;
  }
}
