import { Entidad } from './entidad';

export class Codigo extends Entidad {
  mostrar: boolean;

  constructor(_valor: any) {
    super(_valor);
    this.mostrar = true;
  }

  // TODO: MEJORAR LA VALIDACION O HACER UNA DIFERENCIAL PARA LA BUSQUEDA EN DESCUENTOS DE JORNADA
  get esValido(): boolean {
    return Boolean(this.valor) &&
      this.valor.length >= 2 &&
      !isNaN(this.soloNumero);
  }

  compararCodigo(_texto: any) {
    return Boolean(this.valor) &&
      this.normalizarTexto(this.valor) === this.normalizarTexto(_texto);
  }

  get soloTexto(): string {
    return Boolean(this.valor) ?
      this.valor.match(/[a-z]+|[^a-z]+/gi)[0] : '';
  }

  get soloNumero(): number {
    return Boolean(this.valor) ?
      +this.valor.match(/[a-z]+|[^a-z]+/gi)[1] : NaN;
  }

  private normalizarTexto(_texto: any): string {
    return _texto.replace(' ', '').toUpperCase();
  }
}
