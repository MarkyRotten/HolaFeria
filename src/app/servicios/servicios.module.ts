import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// IMPORTAR SERVICIOS DE FRONT END
import { AuthService } from './auth.service';
import { SeguridadService } from './seguridad.service';
import { NavegadorService } from './navegador.service';
import { JornadaService } from './jornada.service';
import { MedioPagoService } from './medioPago.service';
import { VentaService } from './venta.service';
import { StockService } from './stock.service';
import { ArticuloElimService } from './articuloElim.service';
import { TipoArticuloService } from './tiposArt.service';
import { EstadisticaService } from './estadistica.service';
import { UserService } from './usuario.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    AuthService,
    SeguridadService,
    NavegadorService,
    JornadaService,
    MedioPagoService,
    VentaService,
    StockService,
    ArticuloElimService,
    TipoArticuloService,
    EstadisticaService,
    UserService
  ]
})
export class ServiciosModule { }
