import { Injectable } from '@angular/core';
// IMPORTAR SERVICIOS DE UI
import { AuthService } from './auth.service';
import { NavegadorService } from './navegador.service';
import { TipoArticuloService } from './tiposArt.service';
import { UserService } from './usuario.service';
// IMPORTAR SERVICIOS DE BE
import { JornadaBackService } from '../firebase/jornada-back.service';
// IMPORTAR ENTIDADES
import { Jornada } from '../entidades/jornada';
import { Articulo } from '../entidades/articulo';
import { Descuento } from '../entidades/descuento';
// IMPORTAR UTILIDADES
import Utils from '../configuraciones/utils';

@Injectable()
export class JornadaService {
  jornadaActual: Jornada = new Jornada(null);
  ultimasJornadas: Jornada[] = [];

  constructor(
    private _authService: AuthService,
    private _navService: NavegadorService,
    private _tipoArtService: TipoArticuloService,
    private _userService: UserService,
    private _backService: JornadaBackService
  ) {}

  get jornadaCreada(): boolean {
    return this.jornadaActual.fechaInicio.valor &&
      !this.jornadaActual.fechaFin.valor;
  }

  get jornadaValida(): boolean {
    return Boolean(this.jornadaActual.esValido);
  }

  marcarRutaVentas() {
    this._backService.traer_jornada()
      .then(
        (_jornadaBack: Jornada) => {
          this.jornadaActual = _jornadaBack && !_jornadaBack.fechaFin.valor ?
            _jornadaBack : new Jornada(null);

          const ruta = this.jornadaValida ?
            ['ventas','crear'] : ['ventas','ver'];

          this._navService.procesarRuta(ruta);
        }
      )
      .catch(
        () => this._navService.procesarRuta(['ventas','ver'])
      )
  }

  traerUtimaJornada(): Promise<any> {
    return this._backService.traer_jornada();
  }

  traerDescuentos() {
    return this._tipoArtService.traerListadoTipos();
  }

  iniciarJornada() {
    this.jornadaActual.listaDescuentos.forEach(
      (_descuento, _i) => {
        if(!_descuento.codigo.valor ) {
          this.jornadaActual.listaDescuentos.splice(_i, 1)
        }
      }
    );

    if(this.jornadaActual.listaDescuentos.length === 0) {
      this.jornadaActual.hayDescuentos = 'false';
    }

    this.jornadaActual.fechaInicio.valor = new Date().toISOString();
    this.jornadaActual.iniciadaPor = this._authService.usuarioAuth.nombre.valor;

    this._backService.crear_jornada(this.jornadaActual);

    this.traerUtimaJornada()
      .then(
        (_jornadaBack: Jornada) => {
          this.jornadaActual = _jornadaBack;
        }
      )
      .catch(
        () => { return; }
      )
  }

  actualizarJornada(ventaEfectivo: number, ventaTarjeta: number) {
    this.jornadaActual.montoEfectivo += +ventaEfectivo;
    this.jornadaActual.montoTarjeta += +ventaTarjeta;

    this._backService.mod_jornada(this.jornadaActual);
  }

  terminarJornada() {
    this.jornadaActual.fechaFin.valor = new Date().toISOString();
    this.jornadaActual.cerradaPor = this._authService.usuarioAuth.nombre.valor;

    this._backService.mod_jornada(this.jornadaActual);
  }

  buscarDescuento(descuento: Descuento, i: number) {
    const _nuevoDescuento = this._tipoArtService.articulosEnStock.find(
      art => Utils.compararTexto(art.codigo, descuento.codigo.valor)
    );
    const _noDuplicados =
      _nuevoDescuento &&
      !(
        this.jornadaActual.listaDescuentos.find(
          _descuentoEnLista => {
            return Utils.compararTexto(_nuevoDescuento.codigo, _descuentoEnLista.codigo.valor)
            && Utils.compararTexto(_nuevoDescuento.descripcion, _descuentoEnLista.descripcion.valor);
          })
      );

    if (_noDuplicados) {
      this.jornadaActual.listaDescuentos[i] = new Descuento(_nuevoDescuento.toJS())
      this.jornadaActual.listaDescuentos[i].codigo.mostrar = false;
    }
  }

  agregarDescuento() {
    this.jornadaActual.agregarDescuento();
  }

  quitarDescuento(i: number) {
    this.jornadaActual.quitarDescuento(i);
  }

  setearDescuento(articulo: Articulo) {
    if (this.jornadaActual.hayDescuentos) {
      const _codigo = articulo.codigo.valor;
      const _descuento = this.jornadaActual.listaDescuentos.find(
        _desc => _codigo.includes(_desc.codigo.valor)
      );

      if (_descuento) {
        articulo.descuento.valor = _descuento.descuento.valor;
      }
    }

    return articulo;
  }

  setearVendedor(nombre: any, index: number) {
    const _resultado = this._userService.listaUsuarios.find(
      _user => {
        return _user.nombre.valor.toLowerCase().includes(nombre.toLowerCase());
      }
    )

    const _noHayDuplicados =
      _resultado &&
      !(this.jornadaActual.listaVendedores.find(
        _vend => {
          return _vend.nombre.valor === _resultado.nombre.valor;
        }
      ))

    if (_noHayDuplicados) {
      this.jornadaActual.listaVendedores[index] = _resultado;
      this.jornadaActual.agregarVendedor();
    }
  }

  quitarVendedor(i: any) {
    this.jornadaActual.quitarVendedor(i);
  }
}
