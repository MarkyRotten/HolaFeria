// HACERLO INJECTABLE PARA TOMAR OTROS SERVICIOS
import { Injectable } from '@angular/core';
// IMPORTAR COMPONENTE DE MODAL
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// IMPORTAR COMPONENTE MODAL
import { ModalComponent } from '../componentes/modal/modal.component';
// IMPORTAR ENTIDAD PARA CONFIGURAR LA LOGICA DEL MODAL
import { ModalConfig } from '../entidades/modalConfig';
// IMPORTAR MENSAJES PARA DAR FORMATO A LA CONFIGUNRACION DEL MODAL
import Mensajes from '../configuraciones/mensajes';
// IMPORTAR SERVICIOS
import { AuthService } from './auth.service';
import { NavegadorService } from './navegador.service';
import { JornadaService } from './jornada.service';
import { VentaService } from './venta.service';
import { StockService } from './stock.service';
import { EstadisticaService } from './estadistica.service';
import { UserService } from './usuario.service';
// IMPORTAR JQUERY (HASTA QUE ENCUENTRE ALGO MEJOR)
import * as $ from 'jquery';

@Injectable()
export class SeguridadService {
  constructor(
    private _modal: NgbModal,
    private _authService: AuthService,
    private _navService: NavegadorService,
    private _jornadaService: JornadaService,
    private _ventasService: VentaService,
    private _stockService: StockService,
    private _estadisticaService: EstadisticaService,
    private _usuariosService: UserService,
  ) {}

  navegar(config: any) {
    const self = this;

    if (typeof config.esLlamadaValida === 'string') {
      const path = this.formatPath(config.esLlamadaValida);
      config.esLlamadaValida = this[path.raiz][path.rama];
    }

    config = new ModalConfig(config);

    if (config.esLlamadaValida && !config.necesitaAcceso) {
      this.finalizarRouteo(self, config);
    } else {
      $('navegador').hide();

      const modalSeguro =
        this.generarModal({
          mensajeModal: config.esLlamadaValida ?
            (config.msjModal || Mensajes.realizarAccion) :
            (config.msjError || Mensajes.faltanCampos),
          tipoModal: config.esLlamadaValida ? 'pass' : 'invalid',
          tituloModal: config.esLlamadaValida ? Mensajes.tituloNecesitaPass : Mensajes.tituloErrorAcceso
        });

      modalSeguro.result.then(
        resultado => {
          if (resultado && config.esLlamadaValida) {
            if (this._authService.compararPass(resultado, config.soloAdmin)) {
              this.finalizarRouteo(self, config);
            } else {
              const modalInvalido =
                this.generarModal({
                  mensajeModal: Mensajes.accesoDenegado,
                  tipoModal: 'invalid',
                  tituloModal: Mensajes.tituloErrorAcceso
                });

              return modalInvalido.result.then(
                () => $('navegador').show(),
                () => $('navegador').show()
              )
            }
          }
          $('navegador').show();
        },
        () => $('navegador').show()
      );
    }
  }

  private finalizarRouteo(self: any, config: any) {
    if (config.servicio) {
      const _ruta = this.formatPath(config.servicio);

      if(_ruta.hoja) {
        self[_ruta.raiz][_ruta.rama](`${_ruta.hoja}`);
      } else {
        self[_ruta.raiz][_ruta.rama]();
      }
    }

    if (config.rutaFinal) {
      const _ruta = config.rutaFinal.indexOf('/') === -1 ?
        config.rutaFinal : config.rutaFinal.split('/');

      self._navService.procesarRuta(_ruta);
    }
  }

  private formatPath(ruta: any) {
    ruta = ruta.split('/');

    return {
      raiz: `_${ruta[0]}Service`,
      rama: ruta[1],
      hoja: ruta.length > 2 ? ruta[2] : null
    }
  }

  private generarModal(modalConfig: any): NgbModalRef {
    let _nuevoModal = this._modal.open(ModalComponent, { centered: true });

    _nuevoModal.componentInstance.mensajeModal = modalConfig.mensajeModal;
    _nuevoModal.componentInstance.tipoModal = modalConfig.tipoModal;
    _nuevoModal.componentInstance.tituloModal = modalConfig.tituloModal;

    return _nuevoModal;
  }
}
