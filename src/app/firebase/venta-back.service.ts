import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// IMPORTAR ENTIDADES
import { Venta } from '../entidades/venta';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';

@Injectable({
  providedIn: 'root'
})
export class VentaBackService {
  private _backService: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase
  ) {
    this._backService = this.firebase.list(BD.VENTAS);
  }

  crear_venta(_venta: Venta) {
    this._backService.push(_venta.toJS());
  }
}
