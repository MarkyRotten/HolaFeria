import { TestBed } from '@angular/core/testing';

import { EstadisticaBackService } from './estadistica-back.service';

describe('EstadisticaBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadisticaBackService = TestBed.get(EstadisticaBackService);
    expect(service).toBeTruthy();
  });
});
