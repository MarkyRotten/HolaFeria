import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
// IMPORTAR ENTIDADES
import { PrecioMod } from '../entidades/precioMod';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';

@Injectable({
  providedIn: 'root'
})
export class PreciosModBackService {
  private lista: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase
  ) {
    this.lista = this.firebase.list(BD.PRECIOS_MOD);
  }

  buscar_ventas_borradas() {
    return this.lista.snapshotChanges();
  }

  registrar_stock_viejo(precioMod: PrecioMod) {
    this.lista.push(precioMod.toJS());
  }
}
