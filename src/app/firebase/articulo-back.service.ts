import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { Articulo } from '../entidades/articulo';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';

@Injectable({
  providedIn: 'root'
})
export class ArticuloBackService {
  private lista_articulos: AngularFireList<any>;

  constructor(private firebase: AngularFireDatabase) {
    this.lista_articulos = this.firebase.list(BD.ARTICULOS);
  }

  buscar_articulos() {
    return this.lista_articulos.snapshotChanges();
  }

  insertar_articulo(articulo: Articulo) {
    this.lista_articulos.push(
      articulo.toJS()
    )
  }

  mod_articulo(articulo: Articulo) {
    this.lista_articulos.update(
      articulo.$key, articulo.toJS()
    );
  }

  eliminar_articulos($key: any) {
    this.lista_articulos.remove($key);
  }

  eliminar_bd() {
    this.lista_articulos.remove();
  }
}
