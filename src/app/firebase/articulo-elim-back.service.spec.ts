import { TestBed } from '@angular/core/testing';

import { ArticuloElimBackService } from './articulo-elim-back.service';

describe('ArticuloElimBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticuloElimBackService = TestBed.get(ArticuloElimBackService);
    expect(service).toBeTruthy();
  });
});
