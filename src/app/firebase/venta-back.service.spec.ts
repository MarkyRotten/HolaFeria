import { TestBed } from '@angular/core/testing';

import { VentaBackService } from './venta-back.service';

describe('VentaBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VentaBackService = TestBed.get(VentaBackService);
    expect(service).toBeTruthy();
  });
});
