import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';
import { ArticuloEliminado } from '../entidades/articuloElim';

@Injectable({
  providedIn: 'root'
})
export class ArticuloElimBackService {
  private lista_articulos_elim: AngularFireList<any>;

  constructor(private firebase: AngularFireDatabase) {
    this.lista_articulos_elim = this.firebase.list(BD.ARTICULOS_ELIM);
  }

  buscar_articulos_elim() {
    return this.lista_articulos_elim.snapshotChanges();
  }

  insertar_articulo_elim(articulo: ArticuloEliminado) {
    this.lista_articulos_elim.push(
      articulo.toJS()
    )
  }

  eliminar_bd() {
    this.lista_articulos_elim.remove();
  }
}
