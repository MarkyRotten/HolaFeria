import { TestBed } from '@angular/core/testing';

import { ArticuloBackService } from './articulo-back.service';

describe('ArticuloService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticuloBackService = TestBed.get(ArticuloBackService);
    expect(service).toBeTruthy();
  });
});
