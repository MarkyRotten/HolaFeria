import { Component, Input, AfterViewInit, Renderer2 } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// IMPORTAR ENTIDAD DE TEXTO
import { Texto } from '../../entidades/tipos/texto';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements AfterViewInit{
  @Input() passModal: Texto = new Texto(null);
  @Input() tituloModal: any;
  @Input() mensajeModal: any;
  @Input() tipoModal: any;

  constructor(
    public activeModal: NgbActiveModal,
    private _render: Renderer2
  ) { }

  ngAfterViewInit() {
    if(this.pidePass) {
      this._render.selectRootElement('#input-pass').focus();
    }
  }

  confirmarPass() {
    if (this.passModal.esValido) {
      this.activeModal.close(this.passModal.valor);
    }
  }

  get pidePass(): Boolean {
    return this.tipoModal === 'pass';
  }
}
