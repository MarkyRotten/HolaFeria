// HACERLO INJECTABLE PARA TOMAR OTROS SERVICIOS
import { Injectable } from '@angular/core';
// IMPORTAR COMPONENTE DE MODAL
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// IMPORTAR COMPONENTE MODAL
import { ModalComponent } from '../componentes/modal/modal.component';
// EL CANACTIVATE PARA USARLO COMO GUARDA EN LAS URL PRINCIPALES
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
// IMPORTAR MENSAJES PARA DAR FORMATO A LA CONFIGUNRACION DEL MODAL
import Mensajes from '../configuraciones/mensajes';
// IMPORTAR SERVICIOS
import { AuthService } from '../servicios/auth.service';
// IMPORTAR JQUERY (HASTA QUE ENCUENTRE ALGO MEJOR)
import * as $ from 'jquery';

@Injectable()
export class SeguridadGuard implements CanActivate {
  constructor(
    private _modal: NgbModal,
    private _authService: AuthService,
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    $('navegador').hide();

    const modalAcceso =
      this.generarModal({
        mensajeModal: Mensajes.darAcceso,
        tipoModal: 'pass',
        tituloModal: Mensajes.tituloNecesitaPass
      });

    return modalAcceso.result.then(
      resultado => {
        if (resultado && this._authService.compararPass(resultado, true)) {
          $('navegador').show();
          return true;
        } else {
          if(resultado) {
            const modalInvalido =
              this.generarModal({
                mensajeModal: Mensajes.accesoDenegado,
                tipoModal: 'invalid',
                tituloModal: Mensajes.tituloErrorAcceso
              });

            return modalInvalido.result.then(
              () => {
                $('navegador').show()
                return false;
              },
              () => {
                $('navegador').show()
                return false;
              }
            )
          } else {
            $('navegador').show()
            return false;
          }
        }
      },
      () => {
        $('navegador').show();
        return false;
      }
    );
  }

  private generarModal(modalConfig: any): NgbModalRef {
    let _nuevoModal = this._modal.open(ModalComponent, { centered: true });

    _nuevoModal.componentInstance.mensajeModal = modalConfig.mensajeModal;
    _nuevoModal.componentInstance.tipoModal = modalConfig.tipoModal;
    _nuevoModal.componentInstance.tituloModal = modalConfig.tituloModal;

    return _nuevoModal;
  }
}
