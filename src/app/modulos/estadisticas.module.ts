import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// MODULO => RUTEO DE ANGULAR
import { RoutingModule } from '../app-routing.module';
// IMPORTAR COMPONENTES
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { ListarEstadisticasComponent } from './estadisticas/listar-estadisticas/listar-estadisticas.component';
import { VerEstadisticaComponent } from './estadisticas/ver-estadistica/ver-estadistica.component';
import { VerJornadaComponent } from './estadisticas/ver-jornada/ver-jornada.component';
import { VerVentaComponent } from './estadisticas/ver-venta/ver-venta.component';
import { VerArticulosVendComponent } from './estadisticas/ver-articulos-vend/ver-articulos-vend.component';

@NgModule({
  declarations: [
    EstadisticasComponent,
    ListarEstadisticasComponent,
    VerEstadisticaComponent,
    VerJornadaComponent,
    VerVentaComponent,
    VerArticulosVendComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RoutingModule
  ],
  exports: [
    EstadisticasComponent,
    ListarEstadisticasComponent,
    VerEstadisticaComponent,
    VerJornadaComponent,
    VerVentaComponent,
    VerArticulosVendComponent
  ]
})
export class EstadisticasModule { }
