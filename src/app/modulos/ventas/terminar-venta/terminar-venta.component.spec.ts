import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminarVentaComponent } from './terminar-venta.component';

describe('TerminarVentaComponent', () => {
  let component: TerminarVentaComponent;
  let fixture: ComponentFixture<TerminarVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminarVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminarVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
