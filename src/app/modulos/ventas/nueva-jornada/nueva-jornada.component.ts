import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { SeguridadService } from '../../../servicios/seguridad.service';
import { JornadaService } from '../../../servicios/jornada.service';
// IMPORTAR ENTIDADES
import { Jornada } from '../../../entidades/jornada';
import { Descuento } from '../../../entidades/descuento';
// IMPORTAR MENSAJES
import Mensajes from '../../../configuraciones/mensajes';

@Component({
  selector: 'nueva-jornada',
  templateUrl: './nueva-jornada.component.html',
  styleUrls: ['./nueva-jornada.component.scss']
})
export class NuevaJornadaComponent {
  @HostBinding('class') clases = 'cuerpo';
  nuevaJornada: Jornada;
  descuentos = [];

  constructor(
    private _seguridadService: SeguridadService,
    private _jornadaService: JornadaService,
    ) {
    this.nuevaJornada = this._jornadaService.jornadaActual;
  }

  mostrarDescuentos() {
    if (this.descuentos.length === 0) {
      this.descuentos = this._jornadaService.traerDescuentos();
    }
  }

  buscarDescuento(_desc: Descuento, i: any) {
    this._jornadaService.buscarDescuento(_desc, i)
  }

  agregarDescuento() {
    this._jornadaService.agregarDescuento();
  }

  quitarDescuento(i: any) {
    this._jornadaService.quitarDescuento(i);
  }

  setearVendedor(nombre: any, index: number) {
    this._jornadaService.setearVendedor(nombre, index);
  }

  quitarVendedor(i: any) {
    this._jornadaService.quitarVendedor(i);
  }

  iniciarJornada() {
    this._seguridadService.navegar({
      esLlamadaValida: 'jornada/jornadaValida',
      necesitaAcceso: true,
      msjModal: Mensajes.iniciarJornada,
      servicio: 'jornada/iniciarJornada',
      rutaFinal: 'ventas/crear'
    });
  }
}
