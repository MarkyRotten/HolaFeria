import { Component, HostBinding, Renderer2, AfterViewInit } from '@angular/core';
// IMPORTAR SERVICIOS
import { VentaService } from '../../../servicios/venta.service';
import { JornadaService } from '../../../servicios/jornada.service';
// IMPORTAR ENTIDADES
import { Venta } from '../../../entidades/venta';
import { Articulo } from '../../../entidades/articulo';
import { Jornada } from '../../../entidades/jornada';

@Component({
  selector: 'nueva-venta',
  templateUrl: './nueva-venta.component.html',
  styleUrls: ['./nueva-venta.component.scss']
})
export class NuevaVentaComponent implements AfterViewInit {
  @HostBinding('class') clases = 'cuerpo';
  nuevaVenta: Venta;
  jornada: Jornada;

  constructor(
    private _ventaService: VentaService,
    private _jornadaService: JornadaService,
    private _render: Renderer2
  ) {
    this.nuevaVenta = this._ventaService.ventaEnCurso;
    this.jornada = this._jornadaService.jornadaActual;
  }

  ngAfterViewInit() {
    const articulo = this._ventaService.ventaEnCurso.articulos.length - 1;
    this._render.selectRootElement(`#test-${articulo}`).focus();
  }

  buscarArticuloVenta(_art: Articulo, i: any) {
    this._ventaService.buscarArticulo(_art, i);
  }

  agregarArticulo(i: number) {
    this._ventaService.agregarArticuloVenta();
  }

  quitarArticuloVenta(i: any) {
    this._ventaService.quitarArticulo(i);
  }
}
