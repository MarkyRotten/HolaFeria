import { Component } from '@angular/core';
// IMPORTAR SERVICIOS
import { NavegadorService } from '../../../servicios/navegador.service';
import { UserService } from '../../../servicios/usuario.service';
// IMPORTAR ENTIDAD DE MODULOS (MANEJO DE LA RUTA)
import { ModulosConfig } from '../../../entidades/modulos';
// IMPORTAR COMPONENTE MODAL Y LA ENTIDAD DE CONFIGURACION SEGURA PARA EL MDAL
import { SeguridadService } from '../../../servicios/seguridad.service';
// IMPORTAR MENSAJES DE CONFIGURACION PARA EL MODAL
import Mensajes from '../../../configuraciones/mensajes';

@Component({
  selector: 'botonera',
  templateUrl: './botonera.component.html',
  styleUrls: ['./botonera.component.scss']
})
export class BotoneraComponent {
  modulo: ModulosConfig;

  constructor(
    private _navService: NavegadorService,
    private _seguridadService: SeguridadService,
    private _userService: UserService
  ) {
    this.modulo = this._navService.modulos;
  }

  // VENTAS => VOLVER A LA JORNADA O A LA VENTA
  volverVentas() {
    this._seguridadService.navegar({
      servicio: 'jornada/marcarRutaVentas'
    });
  }

  // VENTAS => INICIAR LA JORNADA
  iniciarJornada() {
    this._seguridadService.navegar({
      esLlamadaValida: 'jornada/jornadaValida',
      necesitaAcceso: true,
      msjModal: Mensajes.iniciarJornada,
      servicio: 'estadistica/iniciarJornada',
      rutaFinal: 'ventas/crear'
    });
  }

  // VENTAS => AGREGAR UN ARTICULO VACIO A LA VENTA
  sumarArticuloVenta() {
    this._seguridadService.navegar({
      servicio: 'ventas/agregarArticuloVenta'
    });
  }

  // VENTAS => IR AL MODULO DE MEDIOS DE PAGO
  irMedioPagos() {
    this._seguridadService.navegar({
      esLlamadaValida: 'ventas/sonArticulosValidos',
      servicio: 'ventas/sumarPrecios',
      rutaFinal: 'ventas/modificar'
    });
  }

  // VENTAS => VOLVER AL LISTADO DE ARTICULOS POR VENDER
  volverListadoArticulos() {
    this._seguridadService.navegar( {
      rutaFinal: 'ventas/crear'
    })
  }

  // VENTAS => REGISTRAR LA NUEVA VENTA
  terminarVenta() {
    this._seguridadService.navegar({
      esLlamadaValida: 'ventas/ventaValida',
      necesitaAcceso: true,
      soloAdmin: false,
      msjModal: Mensajes.terminarVenta,
      servicio: 'ventas/finalizarVenta',
      rutaFinal: 'ventas/crear'
    });
  }

  // STOCK => BUSCAR EN EL LISTADO
  buscarStock(busqueda: any) {
    this._seguridadService.navegar({
      servicio: `stock/buscarArticulos/${busqueda}`
    });
  }

  // STOCK => VOLVER AL LISTADO
  volverStock() {
    this._seguridadService.navegar({
      servicio: 'stock/limpiarListado',
      rutaFinal: 'stock'
    });
  }

  // STOCK => IR A PANTALLA PARA REGISTRAR
  agregarArticulos() {
    this._seguridadService.navegar({
      rutaFinal: 'stock/crear'
    });
  }

  // STOCK => IR A PANTALLA PARA VER PRECIOS CAMBIADOS
  verPreciosCambiados() {
    this._seguridadService.navegar({
      rutaFinal: 'stock/modificar'
    });
  }

  verArticulosBorrados() {
    this._seguridadService.navegar({
      rutaFinal: 'stock/eliminar'
    });
  }

  // STOCK => SUMAR UN ARTICULO NUEVO A LA LISTA
  sumarArticulo() {
    this._seguridadService.navegar({
      servicio: 'stock/sumarAlListado'
    });
  }

  // STOCK => CONFIRMAR O CANCELAR REGISTRO
  registrarArticulos(seRegistra: boolean) {
    if (seRegistra) {
      this._seguridadService.navegar({
        esLlamadaValida: 'stock/stockValido',
        msjError: Mensajes.errorStock,
        necesitaAcceso: true,
        soloAdmin: true,
        msjModal: Mensajes.registrarStock,
        servicio: 'stock/cargarAlStock',
        rutaFinal: 'stock'
      });
    } else {
      this.volverStock();
    }
  }

  // ESTADISTICAS => TERMINAR UNA JORNADA
  terminarJornada() {
    this._seguridadService.navegar({
      esLlamadaValida: 'jornada/jornadaCreada',
      msjError: Mensajes.errorJornada,
      necesitaAcceso: true,
      soloAdmin: true,
      msjModal: Mensajes.terminarJornada,
      servicio: 'jornada/terminarJornada',
      rutaFinal: 'ventas'
    });
  }

  // ESTADISTICAS => VER LOS ARTICULOS VENDIDOS LOS ULTIMOS 15 DIAS
  verArticulosVendidos() {
    this._seguridadService.navegar({
      rutaFinal: 'estadisticas/eliminar'
    })
  }

  // ESTADISTICAS =>
  buscarArticulosVend(busqueda: string) {
    this._seguridadService.navegar({
      servicio: `estadistica/buscarArticulosVend/${busqueda}`
    });
  }

  // ESTADISTICAS => VOLVER AL LISTADO DE ESTADISTICAS
  volverEstadisticas() {
    this._seguridadService.navegar({
      rutaFinal: 'estadisticas'
    });
  }

  // ESTADISTICAS => VOLVER AL LISTADO DE ESTADISTICAS DEL MES
  volverEstadistica() {
    this._seguridadService.navegar({
      servicio: 'estadistica/verDetallesMes'
    });
  }

  // ESTADISTICAS => VOLVER AL LISTADO DE JORNADAS
  volverJornada() {
    this._seguridadService.navegar({
      servicio: 'estadistica/verDetallesJornada'
    });
  }

  // USUARIOS => VOLVER AL LISTADO
  volverUsuarios() {
    this._seguridadService.navegar({
      servicio: 'usuarios/limpiarUsuario',
      rutaFinal: 'usuarios'
    });
  }

  // USUARIO => IR A PANTALLA PARA REGISTRAR
  agregarUsuario() {
    this._seguridadService.navegar({
      rutaFinal: 'usuarios/crear'
    });
  }

  // USUARIO => CONFIRMAR O CANCELAR REGISTRO
  registrarUsuario(seRegistra: boolean) {
    if (seRegistra) {
      this._seguridadService.navegar({
        esLlamadaValida: 'usuarios/usuarioEsValido',
        msjError: Mensajes.errorUsuario,
        necesitaAcceso: true,
        soloAdmin: true,
        msjModal: Mensajes.registrarUsuario,
        servicio: 'usuarios/crearUsuario',
        rutaFinal: 'usuarios'
      });
    } else {
      this.volverUsuarios();
    }
  }

  // USUARIOS => IR A PANTALLA PARA MODIFICAR
  modificarUsuario() {
    const _userId = this._userService.usuarioMod.id;

    this._seguridadService.navegar({
      servicio: 'usuarios/cambiarEstadoMod',
      rutaFinal: `usuarios/modificar/${_userId}`
    });
  }

  // USUARIOS => CONFIRMAR O CANCELAR CAMBIOS
  guardarUsuario(seGuarda: boolean) {
    if (seGuarda) {
      this._seguridadService.navegar({
        esLlamadaValida: 'usuarios/usuarioModEsValido',
        msjError: Mensajes.errorUsuario,
        necesitaAcceso: true,
        soloAdmin: true,
        msjModal: Mensajes.guardarUsuario,
        servicio: 'usuarios/modificarUsuario',
        rutaFinal: 'usuarios'
      });
    } else {
      const _userId = this._userService.usuarioMod.id;

      this._seguridadService.navegar({
        servicio: 'usuarios/cambiarEstadoMod',
        rutaFinal: `usuarios/ver/${_userId}`
      });
    }
  }
}
