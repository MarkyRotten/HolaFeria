import { Component, OnInit, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { NavegadorService } from '../../../servicios/navegador.service';

@Component({
  selector: 'navegador',
  templateUrl: './navegador.component.html',
  styleUrls: ['./navegador.component.scss']
})
export class NavegadorComponent implements OnInit {
  @HostBinding('class') clases = 'col-md-12 position-fixed';

  constructor(
    private _navService: NavegadorService
  ) { }

  // INICIAS EN LA RUTA PRINCIPAL
  ngOnInit() {
    this.volverVentas();
  }

  volverVentas() {
    this.navegar('');
  }

  // NAVEGADOR => METODO COMUN PARA MANEJAR LA RUTA Y POR QUE MODULO SE NAVEGA
  navegar(ruta: any) {
    this._navService.procesarRuta(ruta);
  }
}
