import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { EstadisticaService } from '../../../servicios/estadistica.service';
import { SeguridadService } from '../../../servicios/seguridad.service';
// IMPORTAR ENTIDADES
import { Jornada } from '../../../entidades/jornada';
import { Venta } from '../../../entidades/venta';

@Component({
  selector: 'ver-jornada',
  templateUrl: './ver-jornada.component.html',
  styleUrls: ['./ver-jornada.component.scss']
})
export class VerJornadaComponent {
  @HostBinding('class') clases = 'cuerpo';
  jornada: Jornada;
  ventasDeJornada: Venta[] = [];
  tituloVentas: string;

  constructor(
    private _estadisticaService: EstadisticaService
  ) {
    this.jornada = this._estadisticaService.detallesJornada;
    this.ventasDeJornada = this._estadisticaService.listaVentas;
    this.tituloVentas = this.ventasDeJornada.length > 0 ?
      'Listado de ventas' :
      'No hay ventas cargadas por el momento.';
  }

  verDetallesVenta(i: any) {
    this._estadisticaService.verDetallesVenta(i);
  }
}
