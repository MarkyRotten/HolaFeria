import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarEstadisticasComponent } from './listar-estadisticas.component';

describe('ListarEstadisticasComponent', () => {
  let component: ListarEstadisticasComponent;
  let fixture: ComponentFixture<ListarEstadisticasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarEstadisticasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarEstadisticasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
