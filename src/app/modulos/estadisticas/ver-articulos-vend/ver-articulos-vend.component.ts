import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { EstadisticaService } from '../../../servicios/estadistica.service';
// IMPORTAR ENTIDADES
import { Articulo } from '../../../entidades/articulo';

@Component({
  selector: 'ver-articulos-vend',
  templateUrl: './ver-articulos-vend.component.html',
  styleUrls: ['./ver-articulos-vend.component.scss']
})
export class VerArticulosVendComponent {
  @HostBinding('class') clases = 'cuerpo';
  listaArticulos: Articulo[];
  titulo: string;

  constructor(
    private _estadisticasService: EstadisticaService
  ) {
    this.listaArticulos = this._estadisticasService.listaArticulosVendidos;
    this.titulo = this.listaArticulos.length > 0 ?
      'Listado de articulos vendidos' :
      'No hay articulos vendidos por el momento.';
  }

}
