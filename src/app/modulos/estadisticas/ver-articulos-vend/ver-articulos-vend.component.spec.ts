import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerArticulosVendComponent } from './ver-articulos-vend.component';

describe('VerArticulosVendComponent', () => {
  let component: VerArticulosVendComponent;
  let fixture: ComponentFixture<VerArticulosVendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerArticulosVendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerArticulosVendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
