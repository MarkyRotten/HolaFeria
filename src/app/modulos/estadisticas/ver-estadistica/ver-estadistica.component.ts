import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { EstadisticaService } from '../../../servicios/estadistica.service';
// IMPORTAR ENTIDADES
import { Estadistica } from '../../../entidades/estadistica';
import { Jornada } from '../../../entidades/jornada';

@Component({
  selector: 'ver-estadistica',
  templateUrl: './ver-estadistica.component.html',
  styleUrls: ['./ver-estadistica.component.scss']
})
export class VerEstadisticaComponent {
  @HostBinding('class') clases = 'cuerpo';
  estad: Estadistica;
  jornadasDelMes: Jornada[] = [];
  titulo = 'Jornadas de ';

  constructor(
    private _estadisticaService: EstadisticaService
  ) {
    this.estad = this._estadisticaService.detallesEstad;
    this.jornadasDelMes = this._estadisticaService.listaJornadas;
  }

  verDetallesJornada(i: any) {
    this._estadisticaService.verDetallesJornada(i);
  }
}
