import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// MODULO => RUTEO DE ANGULAR
import { RoutingModule } from '../app-routing.module';
// IMPORTAR COMPONENTES
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ListarUsuariosComponent } from './usuarios/listar-usuarios/listar-usuarios.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { ModificarUsuarioComponent } from './usuarios/modificar-usuario/modificar-usuario.component';

@NgModule({
  declarations: [
    UsuariosComponent,
    ListarUsuariosComponent,
    CrearUsuarioComponent,
    ModificarUsuarioComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RoutingModule
  ],
  exports: [
    UsuariosComponent,
    ListarUsuariosComponent,
    CrearUsuarioComponent,
    ModificarUsuarioComponent,
  ]
})
export class UsuariosModule { }
