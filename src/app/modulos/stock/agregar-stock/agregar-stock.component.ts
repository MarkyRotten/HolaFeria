import { Component, HostBinding, Renderer2, OnDestroy, AfterViewInit } from '@angular/core';
// IMPORTAR SERVICIOS
import { StockService } from '../../../servicios/stock.service';
import { TipoArticuloService } from '../../../servicios/tiposArt.service';
// IMPORTAR ENTIDADES
import { Articulo } from '../../../entidades/articulo';

@Component({
  selector: 'agregar-stock',
  templateUrl: './agregar-stock.component.html',
  styleUrls: ['./agregar-stock.component.scss']
})
export class AgregarStockComponent implements AfterViewInit , OnDestroy {
  @HostBinding('class') clases = 'cuerpo';
  articulos: Articulo[];
  descuentos = [];

  constructor(
    private _render: Renderer2,
    private _stockService: StockService,
    private _tipoArtService: TipoArticuloService
  ) {
    this.articulos = this._stockService.stockPorCargar;
    this.descuentos = this._tipoArtService.traerListadoTipos();
  }

  ngAfterViewInit() {
    try {
      this._render.selectRootElement('#pass').focus();
    } catch(err) {
      const articulo = this._stockService.stockPorCargar.length - 1;
      this._render.selectRootElement('#art-' + articulo).focus();
    }
  }

  componerCodigo(_art: Articulo) {
    const ultimoArt = this.articulos.slice(this.articulos.length - 1)[0];
    _art.codigo.valor = isNaN(_art.codigo.soloNumero) ?
      this._tipoArtService.armarCodigo(_art.codigo.valor, true) :
      null;

    if (_art.codigo.esValido && ultimoArt.codigo.esValido) {
      this._stockService.sumarAlListado();
      _art.mostrar = false;
    }
  }

  quitarArticulo(i: any) {
    const _codigo = this.articulos[i].codigo;

    this.articulos.splice(i, 1);

    if (_codigo.esValido) {
      this._tipoArtService.eliminarTipoArt(_codigo.valor);
    }
  }

  ngOnDestroy() {
    this._stockService.limpiarListado();
    this._tipoArtService.renovarListado();
  }
}
