import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerPreciosModComponent } from './ver-precios-mod.component';

describe('VerPreciosModComponent', () => {
  let component: VerPreciosModComponent;
  let fixture: ComponentFixture<VerPreciosModComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerPreciosModComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerPreciosModComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
