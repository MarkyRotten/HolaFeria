import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerArticulosDelComponent } from './ver-articulos-del.component';

describe('VerArticulosDelComponent', () => {
  let component: VerArticulosDelComponent;
  let fixture: ComponentFixture<VerArticulosDelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerArticulosDelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerArticulosDelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
