import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { UserService } from '../../../servicios/usuario.service';
// IMPORTAR ENTIDADES
import { Usuario } from '../../../entidades/usuario';

@Component({
  selector: 'modificar-usuario',
  templateUrl: './modificar-usuario.component.html',
  styleUrls: ['./modificar-usuario.component.scss']
})
export class ModificarUsuarioComponent {
  @HostBinding('class') clases = 'cuerpo';
  usuarioMod: Usuario;

  constructor(private _userService: UserService) {
    this.usuarioMod = this._userService.usuarioMod;
    this.usuarioMod.pass.valor = null;
  }
}
